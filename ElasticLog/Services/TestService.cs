﻿using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticLog.Services
{
    public class TestService
    {
        private readonly ILogger<TestService> _Logger;
        public TestService(ILogger<TestService> logger)
        {
            _Logger = logger;
        }

        public string Execute()
        {
            _Logger.LogWarning("Warning Happen: {Warning}", "Something happen in here that need observation.");
            Log.Warning("Warning Happen: {Warning}", "Something happen in here that need observation.");
            return "Test Service Complete";
        }
    }
}

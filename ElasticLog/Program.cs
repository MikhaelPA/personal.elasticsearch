﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Exceptions;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Reflection;

namespace ElasticLog
{
    class Program
    {
        static void Main(string[] args)
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            //using Microsoft.Extensions.Configuration;
            //using Microsoft.Extensions.Configuration.Json;
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails()// using Serilog.Exceptions;
                .CreateLogger();

            var backgrounderMachine = new BackgrounderMachine(configuration);

            try
            {
                Log.Information("Application status:{Mode}", "Starting");
                backgrounderMachine.Run();
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Log.Fatal(ex, "Application status:{Mode}", "Terminated Unexpectedly");
            }
            finally
            {
                Log.Information("Application status:{Mode}", "Stopping");
                Log.CloseAndFlush();
            }
        }
        
        private static ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
        {
            // using Serilog.Sinks.Elasticsearch;
            return new ElasticsearchSinkOptions(new Uri(configuration["ElasticConfiguration:Uri"]))
            {
                MinimumLogEventLevel = Serilog.Events.LogEventLevel.Information,
                AutoRegisterTemplate = true,
                IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace(".", "-")}-{DateTime.UtcNow:yyyy-MM}"
            };
        }
    }
}

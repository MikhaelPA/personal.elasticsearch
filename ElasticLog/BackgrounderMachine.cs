﻿using ElasticLog.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticLog
{
    public class BackgrounderMachine
    {
        private readonly IConfiguration Configuration;
        public BackgrounderMachine(IConfiguration configuration)
        {
            this.Configuration = configuration;
            this._Services = new ServiceCollection();
        }

        private IServiceProvider _ServiceProvider;
        private readonly object ServiceProviderSynchron = new object();
        private readonly IServiceCollection _Services;

        /// <summary>
        /// Thread-Safe instance of internal Backgrounder app service resolver.
        /// </summary>
        public IServiceProvider ServiceProvider
        {
            get
            {
                if (_ServiceProvider == null)
                {
                    // Not sure if you really have to make this thread-safe. 
                    lock (ServiceProviderSynchron)
                    {
                        if (_ServiceProvider == null)
                        {
                            this.ConfigureServices(_Services);
                            _ServiceProvider = _Services.BuildServiceProvider();
                        }

                    }
                }

                return _ServiceProvider;
            }
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // this enable Logging using Dependency Injection in each class
            services.AddLogging(setting =>
            {
                setting.AddSerilog();
            });
            services.AddTransient<TestService>();
        }
        public void Run()
        {
            Log.Information("Backgrounder Machine {Status}.", "Start");
            var test = ServiceProvider.GetRequiredService<TestService>();
            Console.WriteLine($"this is just a console: {test.Execute()}");
            Log.Information("Backgrounder Machine {Status}.", "Stop");
        }
    }
}
